#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initialized_var();
    protect_app();
}

void MainWindow::initialized_var()
{
    this->copyright = false;
    this->lreport_PayPal = NULL;
    this->appendPS.balance_toQIWI = 0;
    this->appendPS.balance_toSTEAM = 0;
    this->appendPS.balance_toWebMoney=0;
    this->appendPS.balance_toYandex=0;
    this->appendPS.withCard_toQIWI=0;
    this->appendPS.withCard_toSTEAM=0;
    this->appendPS.withCard_toWebMoney=0;
    this->appendPS.withCard_toYandex=0;
    this->error.access_f_qiwi = true;
    this->error.access_f_steam = true;
    this->error.access_f_webmoney = true;
    this->error.access_f_yandex = true;
    this->error.f_list_payment = true;
    this->error.f_list_report_PayPal = true;
    this->error.path_dir_report = true;
}

void MainWindow::protect_app()
{
    //Защита софта от пиратства
    this->request.setUrl( QUrl("http://m.vk.com/coo_cisnetwork"));
    this->ncopyright = new QNetworkAccessManager(this);

    this->reply_copyright = this->ncopyright->get(request);
    connect(this->ncopyright, SIGNAL(finished(QNetworkReply*)), this, SLOT(finished_copyright(QNetworkReply*)));
}

void MainWindow::finished_copyright(QNetworkReply *reply)
{
    if (reply->error() == QNetworkReply::NoError)
    {
        // Читаем ответ от сервера
        QByteArray bytes = reply->readAll();
        QString html(bytes);
        int pos=0, count=0;
        QRegExp rx("Павел Кадыров");    // примитивное соответствие числам с плавающей точкой
        while ((pos = rx.indexIn(html, pos)) != -1)
        {
            ++count;
            pos += rx.matchedLength();
            break;
        }
        if(count > 0)
        {
            copyright = true;
            qDebug() << "Право использования дается.";
        }
        else
        {
            copyright = false;
            qDebug() << "Право использования не дается.";
        }

    }
    // Произошла какая-то ошибка
    else
    {
        // обрабатываем ошибку
        qDebug() << reply->errorString();
    }
    delete reply;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_quitApp_clicked()
{
    this->close();
}

QList<Accounts> *MainWindow::list_payment(QString file_name)
{
    file_name.replace(QString("/"), QString("//"));
    if( !file_name.isEmpty() ) // Проверка не пустая ли строка пути
    {
        QFile file(file_name); // Создание объекта файла
        QTextStream file_in(&file); // Связывание файла с потоком в текстовом режиме

        file_in.setCodec("UTF-8");
        if( file.open(QIODevice::ReadOnly | QIODevice::Text)) // Проверяет открыт ли файл
        {
            QList<Accounts>* TMP = new QList<Accounts>;
            while( !file_in.atEnd() )
            {
                Accounts T;
                QString f_temp = file_in.readLine();
                QStringList list;
                list = f_temp.split(QRegExp("\\,+"));
                QStringList::const_iterator it = list.begin();
                while( it != list.end() )
                {
                    T.time = *it; it++;
                    T.first_name = *it; it++;
                    T.last_name = *it; it++;
                    T.email = *it; it++;
                    T.channel_name = *it; it++;
                    T.wallet = *it; it++;
                    T.number_wallet = *it; it++;
                    T.account_vk = *it; it++;
                    T.account_skype = *it; it++;
                    T.own_name = *it; it++;
                    T.completed = *it; it++;
                    TMP->push_back(T);
                    //qDebug() << T.time << ' '<< T.first_name << ' ' << T.last_name << ' ' << T.email << ' ' << T.channel_name << ' ' << T.own_name << ' ' << T.wallet << ' ' <<  T.number_wallet << ' ' << T.account_vk << ' ' << T.account_skype << ' ' << T.completed << endl;
                }
            }
            file.close();
            return TMP;
        }
        else
            error.f_list_payment = true;
            //ui->plainText_Result->appendPlainText( "Ошибка открытия файла " + file_name +" для чтения.\n");
    }
    else
        error.f_list_payment = true;
        //ui->plainText_Result->appendPlainText( "Отсутствует путь до файла с реквизитами. \n");
    return 0;
}

QList<report_PayPal> *MainWindow::list_report_PayPal(QString file_name)
{
    file_name.replace(QString("/"), QString("//"));
    if( !file_name.isEmpty() ) // Проверка не пустая ли строка пути
    {
        QFile file(file_name); // Создание объекта файла
        QTextStream file_in(&file); // Связывание файла с потоком в текстовом режиме

        file_in.setCodec("UTF-8");
        if( file.open(QIODevice::ReadOnly | QIODevice::Text)) // Проверяет открыт ли файл
        {
            QList<report_PayPal>* TMP = new QList<report_PayPal>;
            while( !file_in.atEnd() )
            {
                report_PayPal T;
                QString f_temp = file_in.readLine();
                f_temp.remove(QRegExp("(\\s*)"));
                QStringList list;
                list = f_temp.split(QRegExp("\\;+"));
                QStringList::const_iterator it = list.begin();
                while( it != list.end() )
                {
                    T.Channel = *it; it++;
                    T.Paid = *it; it++;
                    TMP->push_back(T);
                }
            }
            file.close();
            return TMP;
        }
        else
            error.f_list_report_PayPal = true;
            //ui->plainText_Result->appendPlainText("Ошибка открытия файла " + file_name + " для чтения.\n");
    }
    else
        error.f_list_report_PayPal = true;
        //ui->plainText_Result->appendPlainText("Отсутствует путь до файла с отчетом о выплатах на PayPal.\n");
    return 0;
}

void MainWindow::create_report_Paid(QString path_name)
{
    QString path_name_origin(path_name);
    path_name.replace(QString("/"), QString("//"));
    if( !path_name.isEmpty() ) // Проверка не пустая ли строка пути
    {
        this->error.path_dir_report = true;
        QFile WebMoney(path_name+"//WebMoney.csv"); // Создание объекта файла WebMoney
        QFile Yandex(path_name+"//Yandex.csv"); // Создание объекта файла Yandex
        QFile QIWI(path_name+"//QIWI.csv"); // Создание объекта файла QIWI
        QFile Steam(path_name+"//Steam.csv"); // Создание объекта файла Steam

        QTextStream f_WebMoney(&WebMoney); // Связывание файла WebMoney с потоком в текстовом режиме
        QTextStream f_Yandex(&Yandex); // Связывание файла Yandex с потоком в текстовом режиме
        QTextStream f_QIWI(&QIWI); // Связывание файла QIWI с потоком в текстовом режиме
        QTextStream f_Steam(&Steam); // Связывание файла Steam с потоком в текстовом режиме

        QTextCodec *text_codec = QTextCodec::codecForName("Windows-1251");
        f_WebMoney.setCodec(text_codec);
        f_Yandex.setCodec(text_codec);
        f_QIWI.setCodec(text_codec);
        f_Steam.setCodec(text_codec);

        QList<create_report> *doc_WebMoney = lst_payment_systems(lpayment,lreport_PayPal,"WebMoney");
        QList<create_report> *doc_Yandex  = lst_payment_systems(lpayment,lreport_PayPal,"Yandex");
        QList<create_report> *doc_QIWI = lst_payment_systems(lpayment,lreport_PayPal,"QIWI Wallet");
        QList<create_report> *doc_Steam = lst_payment_systems(lpayment,lreport_PayPal,"Steam");

        double course_usd_to_rub = ui->course_usd_to_rub->value();
        if(doc_WebMoney->length() >0)
        {
            if( WebMoney.open(QIODevice::WriteOnly | QIODevice::Text)) // Проверяет открыт ли файл
            {
                double pay_system_commmission = ui->internal_commission_webmoney->value();
                double bank_commmission = ui->commission_refill_wmr->value();
                double all_commmission = pay_system_commmission+bank_commmission;
                double common_sum=0;
                for(int i=0; i<doc_WebMoney->length();i++)
                {
                    f_WebMoney << doc_WebMoney->at(i).Number_wallet << ';' << Total_result(doc_WebMoney->at(i).Total,course_usd_to_rub,bank_commmission,pay_system_commmission) << ';' <<  doc_WebMoney->at(i).Description << ';' << doc_WebMoney->at(i).ID << endl;
                    common_sum += doc_WebMoney->at(i).Total.toDouble() * course_usd_to_rub;
                }
                WebMoney.close();
                double ps_balance = common_sum-(common_sum*(all_commmission/100));
                ps_balance = ps_balance + (ps_balance*(pay_system_commmission/100));
                appendPS.balance_toWebMoney = ps_balance;
                appendPS.withCard_toWebMoney = ps_balance+(ps_balance*(bank_commmission/100));
                error.access_f_webmoney = false;
            }
            else
                error.access_f_webmoney = true;
        }
        if(doc_Yandex->length() > 0)
        {
            if( Yandex.open(QIODevice::WriteOnly | QIODevice::Text) ) // Проверяет открыт ли файл
            {
                double pay_system_commmission = ui->internal_commission_yandex->value();
                double bank_commmission = ui->commission_refill_yandex->value();
                double all_commmission = pay_system_commmission+bank_commmission;
                double common_sum=0;
                for(int i=0; i<doc_Yandex->length();i++)
                {
                    f_Yandex << doc_Yandex->at(i).Number_wallet << ';' << Total_result(doc_Yandex->at(i).Total,course_usd_to_rub,bank_commmission,pay_system_commmission) << ';' <<  doc_Yandex->at(i).Description << ';' << doc_Yandex->at(i).ID << endl;
                    common_sum += doc_Yandex->at(i).Total.toDouble() * course_usd_to_rub;
                }
                double ps_balance = common_sum-(common_sum*(all_commmission/100));
                ps_balance = ps_balance + (ps_balance*(pay_system_commmission/100));
                appendPS.balance_toYandex = ps_balance;
                appendPS.withCard_toYandex = ps_balance+(ps_balance*(bank_commmission/100));
                error.access_f_yandex = false;
            }
            else
                error.access_f_yandex = true;
        }
        if(doc_QIWI->length() > 0)
        {
            if( QIWI.open(QIODevice::WriteOnly | QIODevice::Text)) // Проверяет открыт ли файл
            {
                double pay_system_commmission = ui->internal_commission_qiwi->value();
                double bank_commmission = ui->commission_refill_qiwi->value();
                double all_commmission = pay_system_commmission+bank_commmission;
                double common_sum=0;
                for(int i=0; i<doc_QIWI->length();i++)
                {
                    f_QIWI << doc_QIWI->at(i).Number_wallet << ';' << Total_result(doc_QIWI->at(i).Total,course_usd_to_rub,bank_commmission,pay_system_commmission) << ';' <<  doc_QIWI->at(i).Description << ';' << doc_QIWI->at(i).ID << endl;
                    common_sum += doc_QIWI->at(i).Total.toDouble() * course_usd_to_rub;
                }
                QIWI.close();
                double ps_balance = common_sum-(common_sum*(all_commmission/100));
                ps_balance = ps_balance + (ps_balance*(pay_system_commmission/100));
                appendPS.balance_toQIWI = ps_balance;
                appendPS.withCard_toQIWI = ps_balance+(ps_balance*(bank_commmission/100));
                error.access_f_qiwi = false;
            }
            else
                error.access_f_qiwi = true;
        }
        if(doc_Steam->length() > 0)
        {
            if( Steam.open(QIODevice::WriteOnly | QIODevice::Text) ) // Проверяет открыт ли файл
            {
                double pay_system_commmission = ui->internal_commission_steam->value();
                double bank_commmission = ui->commission_refill_steam->value();
                double all_commmission = pay_system_commmission+bank_commmission;
                double common_sum=0;
                for(int i=0; i<doc_Steam->length();i++)
                {
                    f_Steam << doc_Steam->at(i).Number_wallet << ';' << Total_result(doc_Steam->at(i).Total,course_usd_to_rub,bank_commmission,pay_system_commmission) << ';' <<  doc_Steam->at(i).Description << ';' << doc_Steam->at(i).ID << endl;
                    common_sum += doc_Steam->at(i).Total.toDouble() * course_usd_to_rub;
                }
                Steam.close();
                double ps_balance = common_sum-(common_sum*(all_commmission/100));
                ps_balance = ps_balance + (ps_balance*(pay_system_commmission/100));
                appendPS.balance_toSTEAM = ps_balance;
                appendPS.withCard_toSTEAM = ps_balance+(ps_balance*(bank_commmission/100));
                error.access_f_steam = false;
            }
            else
                error.access_f_steam = true;
        }
    }
    else
        error.path_dir_report = false;
        //ui->plainText_Result->appendPlainText("Отсутствует путь до директории создания отчетов.\n");
}

QList<create_report> *MainWindow::lst_payment_systems(QList<Accounts> *lpayment, QList<report_PayPal> *lreport_PayPal, QString payment_system)
{
    QList<create_report> *Result = new QList<create_report>;
    int id=0;
    for( int i=0; i <lreport_PayPal->length(); i++)
    {
        for( int j=0; j <lpayment->length(); j++)
        {
            QString tmp_report( lreport_PayPal->at(i).Channel );
            QString tmp_payment1( lpayment->at(j).channel_name );
            QString tmp_payment2( lpayment->at(j).own_name );
            tmp_report.remove(QRegExp("(\\s*)"));
            tmp_report.remove(QRegExp("(^UC?)"));   // Удаление из идентификатора канала вначале UC
            tmp_payment1.remove(QRegExp("(\\s*)"));
            tmp_payment1.remove(QRegExp("(^UC?)")); // Удаление из идентификатора канала вначале UC
            tmp_payment2.remove(QRegExp("(\\s*)"));

            if( tmp_report.compare(tmp_payment1) == 0 || tmp_report.compare(tmp_payment2) == 0 )
            {
                if( lpayment->at(j).wallet.compare(payment_system,Qt::CaseInsensitive) == 0 && lpayment->at(j).completed.compare("Да") == 0 )
                {
                    create_report T;
                    id++;
                    T.ID = QString::number(id);
                    T.Number_wallet = lpayment->at(j).number_wallet;
                    T.Wallet = payment_system;
                    if(lpayment->at(j).own_name.compare("null", Qt::CaseInsensitive) != 0)
                        T.Description = QString("Выручка канала http://www.youtube.com/%1 за %2 %3 года. Спасибо за сотрудничество с %4.")
                                .arg(lpayment->at(j).own_name)
                                .arg(ui->dateEdit->date().longMonthName(ui->dateEdit->date().month()))
                                .arg(ui->dateEdit->date().year())
                                .arg(ui->lineNameVNetwork->text());
                    else
                        T.Description = QString("Выручка канала http://www.youtube.com/channel/%1 за %2 %3 года. Спасибо за сотрудничество с %4.")
                                .arg(lpayment->at(j).channel_name)
                                .arg(ui->dateEdit->date().longMonthName(ui->dateEdit->date().month()))
                                .arg(ui->dateEdit->date().year())
                                .arg(ui->lineNameVNetwork->text());
                    T.Total = lreport_PayPal->at(i).Paid;
                    Result->push_back(T);
                }

            }
        }
    }
    return Result;
}

double MainWindow::Total_result(QString total,double course_usd_to_rub,double bank_commmission, double pay_system_commmission)
{
    double t = total.toDouble()*course_usd_to_rub;
    double comission = pay_system_commmission+bank_commmission;
    return t-(t*(comission/100)) ; // Сумма партнера в RUB с вычетом всех комиссий
}

void MainWindow::on_file_payment_clicked()
{
    QFileDialog file;
    file_payment = file.getOpenFileName( this, QString("Открыть файл реквизитов"), QString(), QString("Текстовые файлы (*.csv)"));
    if(!file_payment.isEmpty())
    {
        ui->select_file_payment->setEnabled(true);
        emit stateChanged_variable();
    }
    //ui->file_payment->setText("Файл выбран");
}

void MainWindow::on_file_report_PayPal_clicked()
{
    QFileDialog file;
    file_report_PayPal = file.getOpenFileName( this, QString("Открыть файл списка выплат на PayPal"), QString(), QString("Текстовые файлы (*.txt)"));
    if(!file_report_PayPal.isEmpty())
    {
        ui->select_file_report_PayPal->setEnabled(true);
        emit stateChanged_variable();
    }
    //ui->file_report_PayPal->setText("Файл выбран");
    //qDebug() << file_report_payments;
}

void MainWindow::on_pathOutputFiles_clicked()
{
    QFileDialog file;
    path_create_report = file.getExistingDirectory( this, QString("Указать путь для сохранения отчетов"));
    if(!path_create_report.isEmpty())
    {
        ui->select_pathOutputFiles->setEnabled(true);
        emit stateChanged_variable();
    }
    //ui->pathOutputFiles->setText("Путь указан");
    //qDebug() << file_report_payments;
}

void MainWindow::on_generation_files_clicked()
{
    if(this->copyright)
    {
        //ui->plainText_Result->setPlainText("");
       // ui->statusBar->setStatusTip("");
        lpayment = list_payment(file_payment);
        lreport_PayPal = list_report_PayPal(file_report_PayPal);
        create_report_Paid(this->path_create_report);
        ui->reportButton->setEnabled(true);
    }
    else
        //ui->statusBar->setStatusTip("Автор программы защищает свои интересы.");
        qDebug() << "Автор программы защищает свои интересы.";
    /*
    for(int i=0; i<lreport_CMS->length(); i++)
        Gross_Earnings += lreport_CMS->at(i).Gross_Earnings.toDouble();
    ui->plainText_Result->appendPlainText(QString ("Количество каналов в отчетности: %1\nВаловая выручка состовляет: %2\n").arg(lreport_CMS->length()).arg(Gross_Earnings));
    */
}

void MainWindow::stateChanged_variable()
{
    bool select_file_payment = ui->select_file_payment->isEnabled();
    bool select_file_report_PayPal = ui->select_file_report_PayPal->isEnabled();
    bool select_pathOutputFiles = ui->select_pathOutputFiles->isEnabled();
    if( select_file_payment == true && select_file_report_PayPal == true && select_pathOutputFiles == true)
        ui->generation_files->setEnabled(true);
    else
        ui->generation_files->setEnabled(false);
}

void MainWindow::on_reportButton_clicked()
{
    Form_reportWork = new Report_Work(this);
    Form_reportWork->initialized_variable(error,appendPS);
    Form_reportWork->exec();
    Form_reportWork->deleteLater();
}

void MainWindow::on_author_triggered()
{
    creator_app = new author(this);
    creator_app->exec();
    creator_app->deleteLater();
}

void MainWindow::on_about_triggered()
{
    about_app = new about(this);
    about_app->exec();
    about_app->deleteLater();
}
