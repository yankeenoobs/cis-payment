#ifndef REPORT_WORK_H
#define REPORT_WORK_H

#include "struct_list.h"
#include <QDialog>

namespace Ui {
    class Report_Work;
}

class Report_Work : public QDialog
{
    Q_OBJECT

public:
    explicit Report_Work(QWidget *parent = 0);
    Ui::Report_Work* getUi() const;
    void initialized_variable(error_status_report arg1,append_PaySystems arg2);
    ~Report_Work();

private:
    Ui::Report_Work *ui;
    error_status_report err_SR;
    append_PaySystems   appendPS;
};

#endif // REPORT_WORK_H
