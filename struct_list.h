#ifndef STRUCT_LIST
#define STRUCT_LIST


#define VER "2.0"
#include <QString>

struct error_status_report
{
    bool access_f_webmoney; //Статус открытия файла для отчета WebMoney
    bool access_f_yandex; //Статус открытия файла для отчета Yandex
    bool access_f_qiwi; //Статус открытия файла для отчета QIWI
    bool access_f_steam; //Статус открытия файла для отчета Sream
    bool f_list_payment; //Статус открытия файла с Реквизитами
    bool f_list_report_PayPal; //Статус открытия файла с отчетом от PayPal
    bool path_dir_report; // Статус директории сохранения файлов для массовых выплат.
};

struct append_PaySystems
{
    double withCard_toWebMoney;
    double withCard_toYandex;
    double withCard_toQIWI;
    double withCard_toSTEAM;
    double balance_toWebMoney;
    double balance_toYandex;
    double balance_toQIWI;
    double balance_toSTEAM;
};

struct Accounts
{
    QString time;
    QString first_name;
    QString last_name;
    QString email;
    QString channel_name;
    QString own_name;
    QString wallet;
    QString number_wallet;
    QString account_vk;
    QString account_skype;
    QString completed;
};

struct report_PayPal
{
    QString Channel;
    QString Paid;
};

struct create_report
{
    QString Number_wallet;
    QString Wallet;
    QString Total;
    QString Description;
    QString ID;
};

#endif // STRUCT_LIST
