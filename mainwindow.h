#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "report_work.h"
#include "author.h"
#include "about.h"
#include "struct_list.h"
#include <QMainWindow>
#include <QDebug>
#include <QListWidget>
#include <cstdlib>
#include <locale>
#include <vector>
#include <list>
#include <QString>
#include <QFile>
#include <QFileDialog>
#include <QRegExp>
#include <QtXml/QtXml>
#include <QtNetwork>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_quitApp_clicked();
    void on_file_payment_clicked();
    void on_generation_files_clicked();
    void on_file_report_PayPal_clicked();
    void on_pathOutputFiles_clicked();
    void finished_copyright(QNetworkReply* reply);
    void on_reportButton_clicked();
    void stateChanged_variable();
    void on_author_triggered();
    void on_about_triggered();
private:
    Ui::MainWindow *ui;
    Report_Work *Form_reportWork;
    author *creator_app;
    about *about_app;
    error_status_report error;
    append_PaySystems appendPS;
    QList<Accounts>* lpayment;
    bool copyright;
    QNetworkAccessManager *ncopyright;
    QNetworkRequest request;
    QNetworkReply *reply_copyright;
    QList<report_PayPal>* lreport_PayPal;
    QString file_payment;
    QString file_report_PayPal;
    QString path_create_report;
    QList<Accounts> * list_payment(QString file_name);
    QList<report_PayPal> * list_report_PayPal(QString file_name);
    void create_report_Paid(QString path_name);
    QList<create_report> * lst_payment_systems(QList<Accounts>* lpayment, QList<report_PayPal>* lreport_PayPal, QString payment_system);
    double Total_result(QString total,double course_usd_to_rub,double bank_commmission, double pay_system_commmission);
    void initialized_var();
    void protect_app();
};

#endif // MAINWINDOW_H
