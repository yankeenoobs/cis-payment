#include "about.h"
#include "ui_about.h"
#include "struct_list.h"

about::about(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::about)
{
    ui->setupUi(this);
    ui->ver->setText(QString("Версия: %1").arg(VER));
}

about::~about()
{
    delete ui;
}
