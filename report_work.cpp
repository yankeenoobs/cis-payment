#include "report_work.h"
#include "ui_report_work.h"

Report_Work::Report_Work(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Report_Work)
{
    ui->setupUi(this);
}

Ui::Report_Work *Report_Work::getUi() const
{
    return ui;
}

Report_Work::~Report_Work()
{
    delete ui;
}

void Report_Work::initialized_variable(error_status_report arg1,append_PaySystems arg2)
{
    this->err_SR = arg1;
    this->appendPS = arg2;

    ui->withCard_toWebMoney->setValue(appendPS.withCard_toWebMoney);
    ui->withCard_toQIWI->setValue(appendPS.withCard_toQIWI);
    ui->withCard_toYandex->setValue(appendPS.withCard_toYandex);
    ui->withCard_toSTEAM->setValue(appendPS.withCard_toSTEAM);

    ui->balance_toWebMoney->setValue(appendPS.balance_toWebMoney);
    ui->balance_toQIWI->setValue(appendPS.balance_toQIWI);
    ui->balance_toYandex->setValue(appendPS.balance_toYandex);
    ui->balance_toSTEAM->setValue(appendPS.balance_toSTEAM);

    ui->create_status_webmoney->setEnabled(!err_SR.access_f_webmoney);
    ui->create_status_qiwi->setEnabled(!err_SR.access_f_qiwi);
    ui->create_status_yandex->setEnabled(!err_SR.access_f_yandex);
    ui->create_status_steam->setEnabled(!err_SR.access_f_steam);
}
